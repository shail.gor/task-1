import 'dotenv/config';
import { Request, Response, NextFunction } from 'express';
import { customRequest } from '../../environment';

import jwt from 'jsonwebtoken';
import { findAttribute } from '../../components/Author/model/authorModel';
import { INTERNAL_SERVER_ERR, VALIDATION_SERVER_ERR } from '../../utils/constants/constants';
import { response } from '../../utils/helper/helper';
import Logger from '../../utils/logger/logger';
import path from 'path';
import i18n from '../i18n/i18n';

const filename = path.basename(__filename);

export const authentication = async function (
    req: customRequest,
    res: Response,
    next: NextFunction
) {
    try {
        let token: string = req.headers.authorization as string;
        let author = req.body.author;

        if (!token) {
            return response(
                i18n.__('AUTHOR.Jwt_token.Apply'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        let jwtToken: any = jwt.verify(token, process.env.JWT_SECRET_KEY as string);
        let uuid: string = jwtToken.uuid;
        // console.log(jwtToken);
        let verify_uuid = await findAttribute(
            {
                uuid: uuid,
            },
            ['uuid']
        );
        if (!verify_uuid) {
            return response(
                i18n.__('AUTHOR.Jwt_token.not_found'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        req.custom = {};
        req.custom.uuid = uuid;

        // else if (req.body.hasOwnProperty('author')) {
        //     let authors_array = Array.from(author.split(','));
        //     let verify_author = await findAttribute({
        //         id: author
        //     },['id']);
        //     if (!verify_author){
        //         return Response('Atleast one author must b logged in!', undefined, VALIDATION_SERVER_ERR, res);
        //     }
        // }
        next();
    } catch (e: any) {
        if (e.name == 'TokenExpiredError') {
            return response(i18n.__('AUTHOR.Jwt_token.expired'), undefined, undefined, res);
        } else if (e.name == 'JsonWebTokenError') {
            return response(i18n.__('AUTHOR.Jwt_token.invalid'), undefined, undefined, res);
        } else {
            console.log(e);
            Logger.error(filename, req.method, undefined, e.message, undefined);
            return response('internalServerErr', undefined, INTERNAL_SERVER_ERR, res);
        }
    }
};
