import { createServer } from 'http';
import app from './app';
import sequelize from './utils/dbConfig';
import redis from './utils/Redis';

const PORT = 3000;

// const __dirname = path.resolve();

// sequelize.sync();

const server = createServer(app);

(async () => {
    try {
        await sequelize.authenticate();
        console.log('DB connection has been established sucessfully..');

        redis();

        server.listen(PORT, function () {
            console.log('Express listening on port ' + PORT);
        });
    } catch (error) {
        console.log('Unable to connect to the server');
    }
})();
