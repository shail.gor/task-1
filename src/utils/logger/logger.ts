import winston from 'winston';

class Logger {
    public logger: any;
    // static error: any;

    constructor() {
        this.logger = winston.createLogger({
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({ filename: 'src/logger/utils/app.log' }),
            ],
        });
    }

    public error(
        fileName: string,
        method: string,
        uuid: string | undefined,
        msg: string,
        data: any = {}
    ) {
        this.logger.error(
            `filename:${fileName}, Method: ${method}, uuid: ${uuid}, message: ${msg}, data: ${data}`
        );
    }

    public info(
        fileName: string,
        method: string,
        uuid: string | undefined,
        msg: string,
        data: any = {}
    ) {
        this.logger.info(
            `filename:${fileName}, Method: ${method}, uuid: ${uuid}, message: ${msg}, data: ${data}`
        );
    }
}

export default new Logger();
