export const SUCCESS: number = 200;

export const INTERNAL_SERVER_ERR: number = 500;

export const VALIDATION_SERVER_ERR: number = 422;

export const NOT_FOUND_ERR: number = 404;
