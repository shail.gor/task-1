import 'dotenv/config';
import S3 from 'aws-sdk/clients/s3.js';

const bucketName = process.env.AWS_BUCKET_NAME;
const region = process.env.AWS_BUCKET_REGION;
const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.AWS_SECRET_KEY;

const s3 = new S3({
    region,
    accessKeyId,
    secretAccessKey,
});

export async function uploadimageToS3(file: any, data: any) {
    const uploadParams: {
        Bucket: string;
        Key: any;
        Body: any;
    } = {
        Bucket: bucketName as string,
        Key: file,
        Body: data,
    };

    return s3.putObject(uploadParams).promise();
    // return new Promise(resolve => {
    //     s3.putObject(uploadParams)
    // })
}

export async function deleteimageToS3(file: any) {
    const deleteParams: {
        Bucket: string;
        Key: any;
    } = {
        Bucket: bucketName as string,
        Key: file,
    };

    return s3.deleteObject(deleteParams).promise();
}
