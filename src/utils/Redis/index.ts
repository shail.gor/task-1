import { createClient } from 'redis';

export const client = createClient();

export default async () => {
    await client.connect();
    console.log('Redis client Connected!');
};
