import 'dotenv/config';
import fs from 'fs';
// import S3 from 'aws-sdk/clients/s3.js';
import { Response } from 'express';

// export function fsWrite(data) {
//     fs.writeFileSync("./data.json", JSON.stringify(data), function (err) {
//         if (err) throw err;
//     });
// }

// export function fsRead() {
//     return fs.readFileSync("./data.json", { flag: 'a+' }, function (err) {
//         if (err) {
//             console.log('Error: ' + err);
//         }
//     });
// }

export function response(message: string, Data: any, status: number | undefined, res: Response) {
    let response_status: {
        status: number | undefined;
        message: string;
        payload: any;
    } = {
        status: status,
        message: message,
        payload: Data,
    };
    // if (response_status.status == 200) {
    //     response_status.payload = Data
    // }
    return res.send(response_status);
}

export function pagination(
    page: number,
    recordsPerPage: number,
    totalRecords: number,
    data: any,
    sortField: string | undefined,
    sortOrder: any,
    res: Response
) {
    return res.send({
        status: 200,
        message: 'Data are: ',
        payload: data,
        pager: {
            sortField: sortField,
            sortOrder: sortOrder,
            recordsPerPage: recordsPerPage,
            totalRecords: totalRecords,
            page: page,
        },
    });
}

export const getDefaultSortOrder = (sortOrder: string): string => {
    const order: string =
        sortOrder && ['asc', 'desc'].indexOf(sortOrder.toLowerCase()) !== -1
            ? sortOrder.toLowerCase() === 'asc'
                ? 'ASC'
                : 'DESC'
            : 'DESC';
    return order;
};

// const bucketName = process.env.AWS_BUCKET_NAME
// const region = process.env.AWS_BUCKET_REGION
// const accessKeyId = process.env.AWS_ACCESS_KEY
// const secretAccessKey = process.env.AWS_SECRET_KEY

// const s3 = new S3({
//     region,
//     accessKeyId,
//     secretAccessKey
// });

// export async function uploadimageToS3(file, data) {
//     const uploadParams = {
//         Bucket: bucketName,
//         Key: file,
//         Body: data
//     }

//     return s3.putObject(uploadParams).promise();
//     // return new Promise(resolve => {
//     //     s3.putObject(uploadParams)
//     // })
// }

// export async function deleteimageToS3(file) {
//     const deleteParams = {
//         Bucket: bucketName,
//         Key: file
//     };

//     return s3.deleteObject(deleteParams).promise();
// }
