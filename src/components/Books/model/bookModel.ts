// import { Model } from 'sequelize';
import Book from '../schema';
import { Author, booksAuthors } from '../../Author/schema/authorSchema';
import { associateInterface, bookInterface } from '../Types/booktypes';
import { Transaction } from 'sequelize';
let author_fields = [
    'first_name',
    'last_name',
    'email',
    'username',
    'city',
    'country',
    'uuid',
    'createdAt',
    'updatedAt',
    'deletedAt',
];

export async function allBook(
    page: number,
    recordsPerPage: number,
    fields: any,
    condition: any,
    order: any
) {
    let { count, rows } = await Book.findAndCountAll({
        attributes: fields,
        where: condition,
        include: [
            {
                model: Author,
                as: 'authors',
                attributes: author_fields,
                through: { attributes: [] },
            },
        ],
        order: order,
        offset: page,
        limit: recordsPerPage,
    });
    return { count, rows };
}

export async function findByUuid(bookUuid: string, fields: any) {
    try {
        let bookData = await Book.findOne({
            attributes: fields,
            where: { uuid: bookUuid },
            // include: [{
            //     model: Author,
            //     as: 'authors',
            //     attributes: author_fields
            // }]
            include: [
                {
                    model: Author,
                    as: 'authors',
                    attributes: author_fields,
                    through: { attributes: [] },
                },
            ],
        });
        return bookData;
    } catch (error) {
        return false;
    }
}

export async function createBook(
    data: associateInterface,
    transaction: Transaction | undefined = undefined
): Promise<Book | Boolean> {
    try {
        let bookData = await Book.create(data, {
            transaction: transaction ? transaction : undefined,
            include: {
                association: booksAuthors,
            },
        });
        return bookData;
    } catch (error) {
        return false;
    }
}

export async function updateBook(
    data: associateInterface,
    bookUuid: string,
    transaction: Transaction | undefined = undefined
): Promise<any | boolean> {
    try {
        let bookData = await Book.update(data, {
            where: { uuid: bookUuid },
            transaction: transaction ? transaction : undefined,
        });
        return bookData;
    } catch (error) {
        return false;
    }
}

export async function deleteBook(
    bookUuid: string,
    transaction: Transaction | undefined = undefined
): Promise<any | boolean> {
    try {
        let bookData = await Book.destroy({
            where: { uuid: bookUuid },
            transaction: transaction ? transaction : undefined,
        });
        return bookData;
    } catch (error) {
        return false;
    }
}
