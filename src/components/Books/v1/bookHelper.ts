import { Op } from 'sequelize';

class BookHelper {
    getOrderByfield(search: any, sortOrder: any) {
        let orderBy, sortField;
        let condition: any = [];

        if (search) {
            let filter = search.filter;
            for (let key in filter) {
                const data: any = filter[key];
                // console.log(data);

                switch (key) {
                    case 'name':
                        orderBy = [['name', sortOrder]];
                        sortField = 'name';
                        condition.push({
                            [key]: { [Op.like]: `%${data}%` },
                        });
                        break;
                    case 'publisher':
                        orderBy = [['publisher', sortOrder]];
                        sortField = 'publisher';
                        condition.push({
                            [key]: { [Op.like]: `%${data}%` },
                        });
                        break;
                    case 'edition':
                        orderBy = [['edition', sortOrder]];
                        sortField = 'edition';
                        condition.push({
                            [key]: { [Op.like]: `%${data}%` },
                        });
                        break;
                    case 'publish_year':
                        orderBy = [['publish_year', sortOrder]];
                        sortField = 'publish_year';
                        condition.push({
                            [key]: { [Op.like]: `%${data}%` },
                        });
                        break;
                    case 'language':
                        orderBy = [['language', sortOrder]];
                        sortField = 'language';
                        condition.push({
                            [key]: { [Op.like]: `%${data}%` },
                        });
                        break;
                }
            }
        } else {
            orderBy = [['createdAt', sortOrder]];
            sortField = 'createdAt';
        }
        return { orderBy, sortField, condition };
    }
}

export default new BookHelper();
