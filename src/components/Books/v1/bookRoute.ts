import express from 'express';
import authentication from '../../../middleware/authorization';
const Router = express.Router();
import * as book from '../v1/bookController';
import { validation } from './bookValidation';

Router.get('/', function (req, res) {
    res.send('Book API ');
});
Router.get('/books', book.getbooks);

Router.post('/books/pager', book.getbooks);

Router.get('/books/:uuid', book.bookUuid);

Router.post('/books', authentication, validation.Add_Validation, book.bookAdd);

Router.put('/books/:uuid', authentication, validation.Update_Validation, book.bookUpdate);

Router.delete('/books/:uuid', authentication, book.bookDelete);

export default Router;
