import 'dotenv/config';
// import { uuid } from 'uuidv4';

import path from 'path';
import { response, pagination } from '../../../utils/helper';
import constant from '../../../utils/constants';
// import { bookMessage, internalServerErr } from '../../../utils/message.js';

// import { Book } from './bookSchema.js';
import bookModel from '../model';
import { customRequest } from '../../../environment';
import { associateInterface, bookInterface } from '../Types/booktypes';
import { deleteimageToS3, uploadimageToS3 } from '../../../utils/Aws/S3';
import { Response } from 'express';
import i18n from '../../../middleware/i18n/i18n';
import Logger from '../../../utils/logger/logger';
import sequelize from '../../../utils/dbConfig';
import { getDefaultSortOrder } from '../../../utils/helper/helper';
import Helper from './bookHelper';

// export const books = [];
// const bookDetails = JSON.parse(fsRead());

const filename = path.basename(__filename);

let fields = [
    'id',
    'name',
    'author',
    'publisher',
    'edition',
    'publish_year',
    'language',
    'description',
    'uuid',
    'coverimage',
    'createdAt',
    'updatedAt',
    'deletedAt',
];

const getbooks = async function (req: customRequest, res: Response) {
    try {
        // throw new Exception('custom error');
        let { page, recordsPerPage, sortOrder } = req.body;
        let { search } = req.body;
        console.log(search);

        sortOrder = getDefaultSortOrder(sortOrder);
        const { orderBy, sortField, condition } = Helper.getOrderByfield(search, sortOrder);
        page = page ? page : 1;
        recordsPerPage = recordsPerPage ? recordsPerPage : 10;

        if (typeof page !== 'number' || typeof recordsPerPage !== 'number') {
            return response(i18n.__('PAGE'), null, constant.VALIDATION_SERVER_ERR, res);
        }
        let startPage = (page - 1) * recordsPerPage;

        // Database
        const { count, rows } = await bookModel.allBook(
            startPage,
            recordsPerPage,
            fields,
            condition,
            orderBy
        );

        return pagination(page, recordsPerPage, count, rows, sortField, orderBy, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), null, constant.INTERNAL_SERVER_ERR, res);
    }
};
const bookUuid = async function (req: customRequest, res: Response) {
    let bookUuid: string = req.params.uuid;

    try {
        // bookDetails.forEach(function (book) {
        //     if (bookUuid === book.uuid && book.deletedAt === null) {
        //         matchedBook = book;
        //     }
        // });

        // Database
        const bookData = await bookModel.findByUuid(bookUuid, fields);

        if (bookData) {
            Logger.info(filename, req.method, bookUuid, i18n.__('BOOK.List'), bookData);
            return response(i18n.__('BOOK.List'), bookData, constant.SUCCESS, res);
        } else {
            Logger.error(filename, req.method, bookUuid, i18n.__('NOT_FOUND'), undefined);
            return response(i18n.__('NOT_FOUND'), null, constant.NOT_FOUND_ERR, res);
        }
    } catch (e: any) {
        // console.log(e);
        Logger.error(filename, req.method, bookUuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), null, constant.INTERNAL_SERVER_ERR, res);
    }
};

const bookAdd = async function (req: customRequest, res: Response) {
    let transaction;
    try {
        // let body = req.body;
        const { name, author, publisher, edition, publish_year, language, description } = req.body;
        let body: associateInterface = {
            name: name,
            author: author,
            publisher: publisher,
            edition: edition,
            publish_year: publish_year,
            language: language,
            description: description,
            coverimage: undefined,
            authorsBooks: undefined,
        };
        let authors_array: any = JSON.parse(body.author!);
        // console.log(authors_array);
        body.authorsBooks = authors_array.map((a_id: number) => {
            // return {a_id};
            return { authors_id: a_id };
        });
        // console.log(body.authors_id);

        let coverimage = req.files.coverimage ? req.files.coverimage : null;

        if (coverimage == null) {
            body.coverimage = null;
        } else {
            let fileExtension: string = path.extname(coverimage.name);
            let coverimageName = 'img-' + Date.now() + fileExtension;

            // list objects of s3 Buckets
            // var bucketParams = {
            //     Bucket : bucketName,
            //   };
            // s3.listObjects(bucketParams, function (err, data) {
            //     if(err)throw err;
            //     console.log(data);
            //   });
            // Sync
            let bufferFile = Buffer.from(coverimage.data, 'binary');

            let putObjectPromise = await uploadimageToS3(coverimageName, bufferFile);
            // body.coverimage = 'http://smartsense-nodejs-poc.s3.ap-south-1.amazonaws.com/' + coverimageName;

            body.coverimage = coverimageName;
        }
        // Database
        transaction = await sequelize.transaction();
        let bookData = await bookModel.createBook(body, transaction);
        await transaction.commit();

        return response(i18n.__('BOOK.created'), bookData, constant.SUCCESS, res);
    } catch (e: any) {
        if (transaction) await transaction.rollback();
        Logger.error(filename, req.method, req.custom?.uuid as string, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), null, constant.INTERNAL_SERVER_ERR, res);
    }
};

const bookUpdate = async function (req: customRequest, res: Response) {
    let transaction;
    let bookUuid = req.params.uuid;
    try {
        const { name, author, publisher, edition, publish_year, language, description } = req.body;
        let body: bookInterface = {
            name: name,
            author: author,
            publisher: publisher,
            edition: edition,
            publish_year: publish_year,
            language: language,
            description: description,
            coverimage: undefined,
            // authorsBooks: undefined,
        };
        // let authors_array: any = JSON.parse(body.author!);
        // // console.log(authors_array);
        // body.authorsBooks = authors_array.map((a_id: number) => {
        //     // return {a_id};
        //     return { authors_id: a_id };
        // });

        let image: any = await bookModel.findByUuid(bookUuid, ['coverimage']);

        let coverimage = req.files.coverimage ? req.files.coverimage : image.coverimage;

        if (!req.files.coverimage) {
            body.coverimage = image.coverimage;
        } else {
            let fileExtension = path.extname(coverimage.name);
            let coverimageName = 'img-' + Date.now() + fileExtension;

            if (image.coverimage) {
                let url = image.coverimage;
                let imageName = url.substring(url.lastIndexOf('/') + 1);
                let deleteObjectPromise = await deleteimageToS3(imageName);
            }
            let bufferFile = Buffer.from(coverimage.data, 'binary');
            // update in s3 bucket
            let putObjectPromise = await uploadimageToS3(coverimageName, bufferFile);

            // body.coverimage = 'http://smartsense-nodejs-poc.s3.ap-south-1.amazonaws.com/' + coverimageName;
            body.coverimage = coverimageName;

            // fsWrite(bookDetails);

            // Database
        }

        transaction = await sequelize.transaction();
        await bookModel.updateBook(body, bookUuid, transaction);
        await transaction.commit();

        let Updated_data = await bookModel.findByUuid(bookUuid, fields);

        return response(i18n.__('BOOK.updated'), Updated_data, constant.SUCCESS, res);
    } catch (e: any) {
        if (transaction) await transaction.rollback();
        else console.log(e);
        Logger.error(filename, req.method, bookUuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), null, constant.INTERNAL_SERVER_ERR, res);
    }
};

const bookDelete = async function (req: customRequest, res: Response) {
    let transaction;
    let bookUuid: string = req.params.uuid;

    try {
        let image: any = await bookModel.findByUuid(bookUuid, ['coverimage']);

        if (image.coverimage !== null) {
            // Delete in AWs-S3
            let url = image.coverimage;
            let coverimageName = url.substring(url.lastIndexOf('/') + 1);

            // Database
            let deleteObjectPromise = await deleteimageToS3(coverimageName);
            // fsWrite(bookDetails);
        }
        transaction = await sequelize.transaction();
        let removeBook = await bookModel.deleteBook(bookUuid);
        await transaction.commit();
        if (!removeBook) {
            return response(i18n.__('NOT_FOUND'), null, constant.NOT_FOUND_ERR, res);
        }

        return response(i18n.__('BOOK.deleted'), undefined, constant.SUCCESS, res);
    } catch (e: any) {
        if (transaction) await transaction.rollback();
        Logger.error(filename, req.method, bookUuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), null, constant.INTERNAL_SERVER_ERR, res);
    }
};
export { getbooks, bookUuid, bookAdd, bookUpdate, bookDelete };
