import moment from 'moment';
import { Request, Response, NextFunction } from 'express';
import { customRequest } from '../../../environment';
import constant from '../../../utils/constants';
import { response } from '../../../utils/helper/helper';
import i18n from '../../../middleware/i18n/i18n';
import { checkImageSize, checkImageType } from '../../../utils/customValidation';

const validation = {
    Add_Validation: function (req: customRequest, res: Response, next: NextFunction) {
        // let body = _.pick(req.body, 'name', 'author', 'publisher', 'edition', 'publish_year', 'language');
        const { name, author, publisher, edition, publish_year, language, description } = req.body;
        let body = {
            name: name,
            author: author,
            publisher: publisher,
            edition: edition,
            publish_year: publish_year,
            language: language,
            description: description,
        };

        if (!body.name || parseInt(body.name) || body.name.trim().length == 0) {
            return response(
                i18n.__('BOOK.Validation.name'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!body.author) {
            return response(
                i18n.__('BOOK.Validation.Author'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        // if (!body.author || parseInt(body.author) || body.author.trim().length == 0) {
        //     return response("Book Author is required String and enter valid Author", undefined, constant.VALIDATION_SERVER_ERR, res);
        // }
        if (!body.publisher || parseInt(body.publisher) || body.publisher.trim().length == 0) {
            return response(
                i18n.__('BOOK.Validation.publisher'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!body.edition || !parseInt(body.edition)) {
            return response(
                i18n.__('BOOK.Validation.edition'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!body.publish_year || !parseInt(body.publish_year)) {
            return response(
                i18n.__('BOOK.Validation.publish_year.required'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        } else if (body.publish_year > moment().year()) {
            return response(
                i18n.__('BOOK.Validation.publish_year.valid'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!body.language || parseInt(body.language) || body.language.trim().length == 0) {
            return response(
                i18n.__('BOOK.Validation.language'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (
            !body.description ||
            parseInt(body.description) ||
            body.description.trim().length == 0
        ) {
            return response(
                i18n.__('BOOK.Validation.description'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (checkImageSize(req.files.coverimage) === false) {
            return response(
                i18n.__('BOOK.Validation.coverimage.size'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        } else if (checkImageType(req.files.coverimage) === false) {
            return response(
                i18n.__('BOOK.Validation.coverimage.type'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        next();
    },
    Update_Validation: function (req: customRequest, res: Response, next: NextFunction) {
        // let body = _.pick(req.body, 'name', 'author', 'publisher', 'edition', 'publish_year', 'language');
        const { name, author, publisher, edition, publish_year, language, description } = req.body;
        let body = req.body;

        if (body.name && (parseInt(body.name) || name == 0 || body.name.trim().length == 0)) {
            return response(
                'Name must be a string and cannot be null',
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (
            body.author &&
            (parseInt(body.author) || author == 0 || body.author.trim().length == 0)
        ) {
            return response(
                'Author must b a string and cannot be a null',
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (
            body.publisher &&
            (parseInt(body.publisher) || publisher == 0 || body.publisher.trim().length == 0)
        ) {
            return response(
                'publisher must be a string and cannot be a null',
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (body.edition && (body.edition === 0 || !parseInt(body.edition))) {
            return response(
                'Edition must be a number and cannot be a null',
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (
            body.publish_year &&
            (body.publish_year > moment().year() ||
                body.publish_year === 0 ||
                !parseInt(body.publishYear))
        ) {
            return response(
                'Enter valid publishYear number and cannot be a null',
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (
            body.language &&
            (parseInt(body.language) || language == 0 || body.language.trim().length == 0)
        ) {
            return response(
                'Language must be a string and cnnot be a null',
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (
            body.description &&
            (parseInt(body.description) || description == 0 || body.description.trim().length == 0)
        ) {
            return response(
                i18n.__('BOOK.Validation.description'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        if (checkImageSize(req.files.coverimage) === false) {
            return response(
                i18n.__('BOOK.Validation.coverimage.size'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        } else if (checkImageType(req.files.coverimage) === false) {
            return response(
                i18n.__('BOOK.Validation.coverimage.type'),
                undefined,
                constant.VALIDATION_SERVER_ERR,
                res
            );
        }
        next();
    },
};

export { validation };
