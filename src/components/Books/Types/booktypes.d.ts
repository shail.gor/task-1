declare namespace BookEnvironment {
    export interface bookInterface {
        id?: number;
        name?: string;
        author?: string;
        publisher?: string;
        edition?: string;
        publish_year?: string;
        language?: string;
        description?: string;
        uuid?: string;
        coverimage?: string | null;
        createdAt?: Date;
        updatedAt?: Date;
        deletedAt?: Date;
    }

    export interface associateInterface extends bookInterface {
        authorsBooks?: any;
    }
}

export = BookEnvironment;
