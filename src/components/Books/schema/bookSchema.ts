import 'dotenv/config';
import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';

import { sequelize } from '../../../utils/dbConfig/db_config';

export class Book extends Model<InferAttributes<Book>, InferCreationAttributes<Book>> {
    declare id?: number;
    declare name?: string;
    declare author?: string;
    declare publisher?: string;
    declare edition?: string;
    declare publish_year?: string;
    declare language?: string;
    declare description?: string;
    declare uuid?: string;
    declare coverimage?: string | null;
}

Book.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        author: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        publisher: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        edition: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        publish_year: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        language: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        uuid: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false,
        },
        coverimage: {
            type: DataTypes.STRING,
            defaultValue: null,
            get() {
                const imageUrl = this.getDataValue('coverimage');
                if (imageUrl == null) {
                    return imageUrl;
                }
                return process.env.AWS_IMAGE_URL + imageUrl;
            },
        },
    },
    {
        sequelize,
        modelName: 'Book',
        tableName: 'books',
        paranoid: true,
    }
);
