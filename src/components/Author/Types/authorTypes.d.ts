declare namespace AuthorEnvironment {
    export interface authorInterface {
        id?: number;
        first_name?: string;
        last_name?: string;
        email?: string;
        password?: string;
        username?: string;
        city?: string;
        country?: string;
        email_verified?: Boolean;
        uuid?: string;
        otp?: string;
        profile_image?: string | null;
        createdAt?: Date;
        updatedAt?: Date;
        deletedAt?: Date;
    }
}

export = AuthorEnvironment;
