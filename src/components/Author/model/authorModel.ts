import 'dotenv/config';
import { Author } from '../schema/authorSchema';
import sgMail from '@sendgrid/mail';

import jwt from 'jsonwebtoken';
import Book from '../../Books/schema';
import { authorInterface } from '../Types/authorTypes';
import { Op } from 'sequelize';

let book_fields = [
    'name',
    'author',
    'publisher',
    'edition',
    'publish_year',
    'language',
    'description',
    'uuid',
    'coverimage',
    'createdAt',
    'updatedAt',
    'deletedAt',
];

// Sendgrid to send email
export async function sendEmail(
    email: string,
    emailToken: string | undefined,
    otp: string | undefined
) {
    try {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY as string);
        if (emailToken !== undefined) {
            let msg = {
                to: email,
                from: 'shail.gor@smartsensesolutions.com',
                subject: 'Email verification',
                text: 'please verify your account',
                html: `<h1>Account Verification</h1>
                       <p>please click below to verify your account</p>
                       <a href = "http://localhost:3000/author/email-verification/${emailToken}" target="_blank" >click here</a>`,
            };
            // console.log(msg);
            return await sgMail.send(msg);
        } else if (otp !== undefined) {
            let msg = {
                to: email,
                from: 'shail.gor@smartsensesolutions.com',
                subject: 'Forgot Password',
                text: 'Forgot Password',
                html: `<h1>Here is your OTP for Reset your Password valid for 2 min</h1>
                       <h2><strong>${otp}</strong></h2>`,
            };
            // console.log(msg);
            return await sgMail.send(msg);
        }
    } catch (error: any) {
        throw error;
    }
}

export async function allAuthor(page: number, recordsPerPage: number, fields: any) {
    let { count, rows } = await Author.findAndCountAll({
        attributes: fields,
        include: [
            {
                model: Book,
                as: 'books',
                attributes: book_fields,
                through: { attributes: [] },
            },
        ],
        offset: page,
        limit: recordsPerPage,
    });
    return { count, rows };
}

export async function findByUuid(authorUuid: string, fields: any) {
    let authorData: any = await Author.findOne({
        attributes: fields,
        include: [
            {
                model: Book,
                as: 'books',
                attributes: book_fields,
                through: { attributes: [] },
            },
        ],
        where: { uuid: authorUuid },
    });
    return authorData;
}

export async function findAttribute(condition: any, fields: any) {
    let data: any = await Author.findOne({
        attributes: fields,
        where: condition,
    });
    return data;
}

export async function validateUser(email: string, username: string) {
    let message;
    let data: any = await Author.findOne({
        attributes: ['email', 'username'],
        where: {
            [Op.or]: [{ email: email }, { username: username }],
        },
    });
    if (data) {
        if (data.email == email) {
            message = 'Email address alredy in use!';
        } else if (data.username == username) {
            message = 'Username alredy in use!';
        }
    }
    return message;
}

// export async function emailVerification(token) {
//     let data = await Author.findAll({
//         where: {
//             [Op.or]: [
//                 { verification_token: token },
//                 {
//                     email_expiry: { [Op.gt]: sequelize.literal(`now() - INTERVAL 2 MINUTE`) }
//                 },
//             ]
//         },
//         logging: console.log,
//     });
//     return data;
// }

export async function createAuthor(data: authorInterface) {
    let authorData: any = await Author.create(data);
    return authorData;
}

export async function updateAuthor(data: authorInterface, authorUuid: string) {
    let authorData: any = await Author.update(data, {
        where: { uuid: authorUuid },
    });
    return authorData;
}

export async function deleteAuthor(authorUuid: string) {
    let authorData: any = await Author.destroy({
        where: { uuid: authorUuid },
    });
    return authorData;
}

export async function jwtToken(uuid: string) {
    let token: any = jwt.sign(
        {
            uuid: uuid,
        },
        process.env.JWT_SECRET_KEY as string,
        {
            expiresIn: '2h',
        }
    );
    return token;
}

export function generateOtp() {
    // let otp = Math.floor(Math.random() * 1000000) + 1000000;
    var digits = '0123456789';
    let otp: string = '';
    for (let i = 0; i < 6; i++) {
        otp += digits[Math.floor(Math.random() * 10)];
    }
    console.log(otp);
    return otp;
}
