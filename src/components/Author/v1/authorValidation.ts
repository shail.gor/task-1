import { VALIDATION_SERVER_ERR } from '../../../utils/constants/constants.js';
import { response } from '../../../utils/helper/helper.js';
import { Request, Response, NextFunction } from 'express';
import { customRequest } from '../../../environment';
import { authorInterface } from '../Types/authorTypes.js';
import { checkImageSize, checkImageType, isEmpty, isString } from '../../../utils/customValidation';
import i18n from '../../../middleware/i18n/i18n';

export const validation = {
    addValidation: function (req: customRequest, res: Response, next: NextFunction) {
        let { first_name, last_name, email, password, username, city, country } = req.body;
        console.log(typeof first_name);

        if (!first_name) {
            console.log(i18n.__('AUTHOR.Validation.first_name.required'));
            return response(
                i18n.__('AUTHOR.Validation.first_name.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(first_name) || first_name == 0 || first_name.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.first_name.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!last_name) {
            return response(
                i18n.__('AUTHOR.Validation.last_name.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(last_name) || last_name == 0 || last_name.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.last_name.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!email) {
            return response(
                i18n.__('AUTHOR.Validation.email.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(email) || email == 0 || email.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.email.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!password) {
            return response(
                i18n.__('AUTHOR.Validation.password.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(password) || password == 0 || password.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.password.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!username) {
            return response(
                i18n.__('AUTHOR.Validation.username.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(username) || username == 0 || username.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.username.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!city) {
            return response(
                i18n.__('AUTHOR.Validation.city.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(city) || city == 0 || city.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.city.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (!country) {
            return response(
                i18n.__('AUTHOR.Validation.country.required'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (parseInt(country) || country == 0 || country.trim().length == 0) {
            return response(
                i18n.__('AUTHOR.Validation.country.valid'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        if (checkImageSize(req.files.profile_image) === false) {
            return response(
                i18n.__('AUTHOR.Validation.profile_image.size'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (checkImageType(req.files.profile_image) === false) {
            return response(
                i18n.__('AUTHOR.Validation.profile_image.type'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        next();
    },
    updateValidation: function (req: customRequest, res: Response, next: NextFunction) {
        let { first_name, last_name, email, password, username, city, country } = req.body;

        if (req.body.first_name) {
            if (parseInt(first_name) || first_name == 0 || first_name.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.first_name.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (req.body.last_name) {
            if (parseInt(last_name) || last_name == 0 || last_name.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.last_name.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (req.body.email) {
            if (parseInt(email) || email == 0 || email.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.email.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (req.body.password) {
            if (parseInt(password) || password == 0 || password.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.password.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (req.body.username) {
            if (parseInt(username) || username == 0 || username.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.username.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (req.body.city) {
            if (parseInt(city) || city == 0 || city.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.city.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (req.body.country) {
            if (parseInt(country) || country == 0 || country.trim().length == 0) {
                return response(
                    i18n.__('AUTHOR.Validation.country.valid'),
                    undefined,
                    VALIDATION_SERVER_ERR,
                    res
                );
            }
        }
        if (checkImageSize(req.files.profile_image) === false) {
            return response(
                i18n.__('AUTHOR.Validation.profile_image.size'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        } else if (checkImageType(req.files.profile_image) === false) {
            return response(
                i18n.__('AUTHOR.Validation.profile_image.type'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        next();
    },
    login_validation: function (req: customRequest, res: Response, next: NextFunction) {
        let { email, password }: authorInterface = req.body;

        if (!email) {
            return response("Author's email id is required", undefined, VALIDATION_SERVER_ERR, res);
        }
        if (!password) {
            return response('Password is required', undefined, VALIDATION_SERVER_ERR, res);
        }
        next();
    },
    forgotPassword_validation: function (req: Request, res: Response, next: NextFunction) {
        let { name } = req.body;

        if (!name) {
            return response(
                "Author's username/email is required",
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
        next();
    },
    otpVerify_validation: function (req: Request, res: Response, next: NextFunction) {
        let { uuid, otp } = req.body;

        if (!uuid) {
            return response('Id is required', undefined, VALIDATION_SERVER_ERR, res);
        }
        if (!otp) {
            return response('otp is required', undefined, VALIDATION_SERVER_ERR, res);
        }
        next();
    },
    resendOtp_validation: function (req: Request, res: Response, next: NextFunction) {
        let uuid = req.body.uuid;
        console.log('aa', uuid);

        if (!uuid) {
            return response('Id is required', undefined, VALIDATION_SERVER_ERR, res);
        }
        next();
    },
    resetPassword_validation: function (req: Request, res: Response, next: NextFunction) {
        let { uuid, new_password } = req.body;

        if (!uuid) {
            return response('Id is required', undefined, VALIDATION_SERVER_ERR, res);
        }
        if (!new_password) {
            return response('please insert new password', undefined, VALIDATION_SERVER_ERR, res);
        }
        next();
    },
};
