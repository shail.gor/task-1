import {
    INTERNAL_SERVER_ERR,
    NOT_FOUND_ERR,
    SUCCESS,
    VALIDATION_SERVER_ERR,
} from '../../../utils/constants/constants';
import { deleteimageToS3, uploadimageToS3 } from '../../../utils/Aws/S3';
import { pagination, response } from '../../../utils/helper';
import bcrypt from 'bcrypt';

import authorModel from '../model';
import { Author } from '../schema';
import path from 'path';
import { Op } from 'sequelize';
import { v4 as uuid_v4 } from 'uuid';
import { customRequest } from '../../../environment';
import { authorInterface } from '../Types/authorTypes';
import { Response } from 'express';
import i18n from '../../../middleware/i18n/i18n';
import { client } from '../../../utils/Redis';
import Logger from '../../../utils/logger/logger';

const filename: any = path.basename(__filename);

let field = [
    'first_name',
    'last_name',
    'email',
    'username',
    'city',
    'country',
    'uuid',
    'email_verified',
    'profile_image',
    'createdAt',
    'updatedAt',
    'deletedAt',
];

export const getAuthor = async function (req: customRequest, res: Response) {
    try {
        // throw new Error('custom error');
        let page: number = req.body.page ? req.body.page : 1;
        let recordsPerPage: number = req.body.recordsPerPage ? req.body.recordsPerPage : 10;

        if (typeof page !== 'number' || typeof recordsPerPage !== 'number') {
            return response(i18n.__('PAGE'), undefined, VALIDATION_SERVER_ERR, res);
        }

        let startPage = (page - 1) * recordsPerPage;

        const { count, rows } = await authorModel.allAuthor(startPage, recordsPerPage, field);

        Logger.info(filename, req.method, undefined, i18n.__('AUTHOR.List'), rows);
        return pagination(page, recordsPerPage, count, rows, undefined, undefined, res);
    } catch (e: any) {
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(res.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const authorUuid = async function (req: customRequest, res: Response) {
    try {
        let authorUuid: string = req.params.uuid;

        const authorData = await authorModel.findByUuid(authorUuid, field);

        if (authorData) {
            Logger.info(filename, req.method, authorUuid, i18n.__('AUTHOR.List'), authorData);
            return response(i18n.__('AUTHOR.List'), authorData, SUCCESS, res);
        } else {
            Logger.error(filename, req.method, authorUuid, i18n.__('NOT_FOUND'), undefined);
            return response(i18n.__('NOT_FOUND'), undefined, NOT_FOUND_ERR, res);
        }
    } catch (e: any) {
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const SignUpAuthor = async function (req: customRequest, res: Response) {
    try {
        let { first_name, last_name, email, password, username, city, country } = req.body;
        let body: authorInterface = {
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: password,
            username: username,
            city: city,
            country: country,
        };

        let verification_token = uuid_v4();

        let profile_image = req.files.profile_image ? req.files.profile_image : null;

        let UniqueUser = await authorModel.validateUser(email, username);
        if (UniqueUser) {
            return response(UniqueUser, undefined, VALIDATION_SERVER_ERR, res);
        }
        // let date = new Date()
        // body.email_expiry = date;
        // console.log(body.is_email_validtime);

        if (profile_image == null) {
            body.profile_image = null;
        } else {
            let imageExtension = path.extname(profile_image.name);
            let profile_imageName = 'img-' + Date.now() + imageExtension;

            let bufferFile = Buffer.from(profile_image.data, 'binary');

            let putObjectPromise = await uploadimageToS3(profile_imageName, bufferFile);

            body.profile_image = profile_imageName;
        }
        let createData = await authorModel.createAuthor(body);

        await client.hSet(verification_token, { uuid: createData.uuid });
        await client.expire(verification_token, 2 * 60);

        let emailVerification = await authorModel.sendEmail(email, verification_token, undefined);

        Logger.info(filename, req.method, createData.uuid, i18n.__('AUTHOR.created'), createData);
        return response(i18n.__('AUTHOR.created'), createData, SUCCESS, res);
    } catch (e: any) {
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const emailVerification = async function (req: customRequest, res: Response) {
    try {
        let emailToken = req.params.token;
        let verify_token = await client.hGet(emailToken, 'uuid');

        console.log(verify_token);
        // let data = await authorModel.emailVerification(emailToken);
        // console.log(data);
        // if (data.length !== 0) {
        if (verify_token) {
            await client.del('verification_token');
            let authorData: any = await Author.update(
                {
                    email_verified: true,
                },
                {
                    where: {
                        email_verified: false,
                    },
                }
            );
            console.log(authorData);
            if (authorData == false) {
                return response(
                    i18n.__('AUTHOR.EmailVerification.alreadyVerify'),
                    undefined,
                    SUCCESS,
                    res
                );
            } else {
                return response(
                    i18n.__('AUTHOR.EmailVerification.verified'),
                    undefined,
                    SUCCESS,
                    res
                );
            }
        } else {
            return response(
                i18n.__('AUTHOR.EmailVerification.expired'),
                undefined,
                VALIDATION_SERVER_ERR,
                res
            );
        }
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const loginAuthor = async function (req: customRequest, res: Response) {
    try {
        // throw new Error('custom error');
        let { email, password }: { email: authorInterface; password: string | Buffer } = req.body;

        let user: any = await authorModel.findAttribute({ email: email }, [
            'uuid',
            'password',
            'email_verified',
        ]);
        console.log(user.email_verified);

        if (user.email_verified) {
            if (user) {
                // console.log(user.password);

                let validatePwd = bcrypt.compareSync(password, user.password);
                // console.log(validatePwd);
                if (validatePwd) {
                    let jwtToken = await authorModel.jwtToken(user.uuid);

                    await client.hSet(user.uuid, { jwt_token: jwtToken });
                    await client.expire(user.uuid, 2 * 60 * 60);

                    // let update = await authorModel.updateAuthor({ jwt_token: jwtToken }, user.uuid);

                    Logger.info(
                        filename,
                        req.method,
                        user.uuid,
                        i18n.__('AUTHOR.login.success'),
                        jwtToken
                    );
                    return response(i18n.__('AUTHOR.login.success'), jwtToken, SUCCESS, res);
                } else {
                    return response(
                        i18n.__('AUTHOR.login.pwd-wrong'),
                        undefined,
                        VALIDATION_SERVER_ERR,
                        res
                    );
                }
            }
            return response(i18n.__('AUTHOR.login.user_not_found'), undefined, NOT_FOUND_ERR, res);
        }
        return response(i18n.__('AUTHOR.login.verify'), undefined, NOT_FOUND_ERR, res);
    } catch (e: any) {
        // console.log(e);
        // loggers.error(
        //     `status: ${e.status || 500} - ${e.message} - ${req.originalUrl} - ${req.method}`
        // );
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const logoutAuthor = async function (req: customRequest, res: Response) {
    let token = req.headers.authorization;
    let uuid: any = req.custom?.uuid;
    try {
        console.log(token);
        // let verify_token = await authorModel.findAttribute({ jwt_token: token }, ['jwt_token', 'uuid']);

        let verify_token = await client.hGet(uuid, 'jwt_token');

        console.log(verify_token);
        if (verify_token == token) {
            await client.del(uuid);
            let jwtToken = null;
            // let update = await authorModel.updateAuthor({ jwt_token: jwtToken }, verify_token.uuid);
            Logger.info(filename, req.method, uuid, i18n.__('AUTHOR.Logout.logout'), undefined);
            return response(i18n.__('AUTHOR.Logout.logout'), undefined, SUCCESS, res);
        }
        return response(i18n.__('AUTHOR.Logout.login'), undefined, VALIDATION_SERVER_ERR, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, uuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const forgotPassword = async function (req: customRequest, res: Response) {
    try {
        let { name } = req.body;

        let user = await authorModel.findAttribute(
            {
                [Op.or]: [{ email: name }, { username: name }],
            },
            ['uuid', 'email']
        );
        if (user) {
            let otp = authorModel.generateOtp();
            let sendEmail = await authorModel.sendEmail(user.email, undefined, otp);

            await client.hSet(user.uuid, { otp: otp });
            await client.expire(user.uuid, 2 * 60);

            // let update = await authorModel.updateAuthor({
            //     otp: otp,
            //     otp_expiry: otp_expiry
            // }, user.uuid);
            Logger.info(
                filename,
                req.method,
                user.uuid,
                i18n.__('AUTHOR.Forgot.otpSent'),
                undefined
            );
            return response(i18n.__('AUTHOR.Forgot.otpSent'), { uuid: user.uuid }, SUCCESS, res);
        }
        return response(i18n.__('AUTHOR.Forgot.user_not_found'), undefined, NOT_FOUND_ERR, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, undefined, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const verifyOtp = async function (req: customRequest, res: Response) {
    let { uuid, otp }: authorInterface = req.body;
    try {
        let checkUuid = await authorModel.findAttribute({ uuid: uuid }, ['uuid']);
        if (checkUuid != null) {
            // let checkOtp = await authorModel.findAttribute({
            //     otp_expiry: {
            //         [Op.gt]: sequelize.literal(`now() - INTERVAL 2 MINUTE`)
            //     }
            // }, ['otp']);
            // let checkOtp = await authorModel.redishClientGet('otp');
            let checkOtp = await client.hGet(checkUuid.uuid, 'otp');
            console.log(checkOtp);
            if (checkOtp == otp) {
                console.log('hh');

                await client.del(checkUuid.uuid);
                // let update = await authorModel.updateAuthor({ otp: null }, checkUuid.uuid);
                Logger.info(
                    filename,
                    req.method,
                    uuid,
                    i18n.__('AUTHOR.verifyOtp.verified'),
                    undefined
                );
                return response(i18n.__('AUTHOR.verifyOtp.verified'), undefined, SUCCESS, res);
            } else if (checkOtp == null) {
                return response(i18n.__('AUTHOR.verifyOtp.expired'), undefined, undefined, res);
            }
        }
        return response(i18n.__('AUTHOR.verifyOtp.incorrect'), undefined, undefined, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, uuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const resendOtp = async function (req: customRequest, res: Response) {
    let { uuid }: authorInterface = req.body;
    try {
        let checkUuid = await authorModel.findAttribute(
            {
                uuid: uuid,
            },
            ['uuid', 'email']
        );
        if (checkUuid) {
            let otp = authorModel.generateOtp();
            // let date = new Date()
            // let otp_expiry = date;
            let sendEmail = await authorModel.sendEmail(checkUuid.email, undefined, otp);

            await client.hSet(checkUuid.uuid, { otp: otp });
            await client.expire('OTP', 2 * 60);

            // let update = await authorModel.updateAuthor({
            //     otp: otp,
            //     otp_expiry: otp_expiry
            // }, checkUuid.uuid);
            Logger.info(filename, req.method, uuid, i18n.__('AUTHOR.OtpResend.sent'), undefined);
            return response(i18n.__('AUTHOR.OtpResend.sent'), undefined, SUCCESS, res);
        }
        return response(i18n.__('AUTHOR.OtpResend.not_found'), undefined, NOT_FOUND_ERR, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, uuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const resetPassword = async function (req: customRequest, res: Response) {
    let { uuid, new_password }: { uuid: authorInterface; new_password: string } = req.body;
    try {
        console.log(new_password);

        let check = await authorModel.findAttribute({ uuid: uuid }, ['uuid', 'otp']);
        if (check) {
            if (check.otp === null) {
                let update = await authorModel.updateAuthor({ password: new_password }, check.uuid);
                Logger.info(
                    filename,
                    req.method,
                    check.uuid,
                    i18n.__('AUTHOR.Reset_pwd.success'),
                    undefined
                );
                return response(i18n.__('AUTHOR.Reset_pwd.success'), undefined, SUCCESS, res);
            }
            return response(i18n.__('AUTHOR.Reset_pwd.verify'), undefined, undefined, res);
        }
        return response(i18n.__('AUTHOR.Reset_pwd.incorrect'), undefined, undefined, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, uuid as string, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const updateAuthor = async function (req: customRequest, res: Response) {
    let authorUuid = req.params.uuid;
    try {
        let { first_name, last_name, email, password, username, city, country } = req.body;
        let body: authorInterface = {
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: password,
            username: username,
            city: city,
            country: country,
        };
        let image = await authorModel.findByUuid(authorUuid, ['profile_image']);

        if (req.body.email || req.body.username) {
            let UniqueUser = await authorModel.validateUser(email, username);
            if (UniqueUser) {
                return response(UniqueUser, undefined, VALIDATION_SERVER_ERR, res);
            }
        }
        if (req.files.profile_image) {
            let profile_image = req.files.profile_image
                ? req.files.profile_image
                : image.profile_image;

            if (!req.files.profile_image) {
                body.profile_image = profile_image;
            } else {
                let imageExtension = path.extname(profile_image.name);
                let profile_imageName = 'img-' + Date.now() + imageExtension;

                if (image.profile_image) {
                    let url = image.profile_image;
                    let imageName = url.substring(url.lastIndexOf('/') + 1);

                    let deleteObjectPromise = await deleteimageToS3(imageName);
                }
                let bufferFile = Buffer.from(profile_image.data, 'binary');

                let putObjectPromise = await uploadimageToS3(profile_imageName, bufferFile);

                body.profile_image = profile_imageName;
            }
        }

        let authorData = await authorModel.updateAuthor(body, authorUuid);
        console.log(authorData);
        let Updated_data = await authorModel.findByUuid(authorUuid, field);

        Logger.info(filename, req.method, authorUuid, i18n.__('AUTHOR.updated'), Updated_data);

        return response(i18n.__('AUTHOR.updated'), Updated_data, SUCCESS, res);
    } catch (e: any) {
        console.log(e);
        Logger.error(filename, req.method, authorUuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};

export const deleteAuthor = async function (req: customRequest, res: Response) {
    let authorUuid = req.params.uuid;
    try {
        let image = await authorModel.findByUuid(authorUuid, ['profile_image']);

        let removeauthor = await authorModel.deleteAuthor(authorUuid);
        if (!removeauthor) {
            return response(i18n.__('NOT_FOUND'), undefined, NOT_FOUND_ERR, res);
        }

        if (image.profile_image !== null) {
            let url = image.profile_image;
            let imageName = url.substring(url.lastIndexOf('/') + 1);

            let deleteObjectPromise = await deleteimageToS3(imageName);
        }
        Logger.info(filename, req.method, authorUuid, i18n.__('AUTHOR.deleted'), undefined);
        return response(i18n.__('AUTHOR.deleted'), removeauthor, SUCCESS, res);
    } catch (e: any) {
        Logger.error(filename, req.method, authorUuid, e.message, undefined);
        return response(i18n.__('INTERNAL_SERVER_ERR'), undefined, INTERNAL_SERVER_ERR, res);
    }
};
