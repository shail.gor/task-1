import express from 'express';
import authentication from '../../../middleware/authorization/index';
const Router = express.Router();
import * as author from './authorController';
import { validation } from './authorValidation';

Router.get('/', function (req, res) {
    res.send('Author Data');
});
Router.get('/author', author.getAuthor);

Router.get('/author/email-verification/:token', author.emailVerification);

Router.get('/author/:uuid', author.authorUuid);

Router.post('/signUp', validation.addValidation, author.SignUpAuthor);

Router.post('/author/login', validation.login_validation, author.loginAuthor);

Router.post('/author/forgotPassword', validation.forgotPassword_validation, author.forgotPassword);

Router.post('/author/verifyOtp', validation.otpVerify_validation, author.verifyOtp);

Router.post('/author/resendOtp', validation.resendOtp_validation, author.resendOtp);

Router.post('/author/resetPassword', validation.resetPassword_validation, author.resetPassword);

Router.post('/author/logout', authentication, author.logoutAuthor);

Router.put('/author/:uuid', authentication, validation.updateValidation, author.updateAuthor);

Router.delete('/author/:uuid', authentication, author.deleteAuthor);

export default Router;
