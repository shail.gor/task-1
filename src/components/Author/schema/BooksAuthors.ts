import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import { sequelize } from '../../../utils/dbConfig/db_config';
// import { Author } from './authorSchema.js';
// import { Book } from '../../Books/schema/bookSchema.js';

export class BooksAuthors extends Model<
    InferAttributes<BooksAuthors>,
    InferCreationAttributes<BooksAuthors>
> {
    declare authors_id: number;
    declare books_id: number;
}

BooksAuthors.init(
    {
        authors_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Author',
                key: 'id',
            },
        },
        books_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Book',
                key: 'id',
            },
        },
    },
    {
        sequelize,
        tableName: 'books_authors',
    }
);

// M - M relation
// Author.belongsToMany(Book, {
//     through: Books_Authors
// });
// Book.belongsToMany(Author, {
//     through: Books_Authors
// });
