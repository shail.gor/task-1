import 'dotenv/config';
import bcrypt from 'bcrypt';

import Book from '../../Books/schema/index';
import { BooksAuthors } from './BooksAuthors';

import { sequelize } from '../../../utils/dbConfig/db_config';
import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';

export class Author extends Model<InferAttributes<Author>, InferCreationAttributes<Author>> {
    declare id?: number;
    declare first_name?: string;
    declare last_name?: string;
    declare email?: string;
    declare password?: string;
    declare username?: string;
    declare city?: string;
    declare country?: string;
    declare email_verified?: Boolean;
    declare uuid?: string;
    declare profile_image?: String | null;

    // declare readonly createdAt: CreationOptional<Date>;
    // declare readonly updatedAt: CreationOptional<Date>;
    // declare readonly deletedAt: CreationOptional<Date>;
}

Author.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        first_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        last_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            set: function (value: string) {
                let salt: string | Buffer = bcrypt.genSaltSync(10);
                let hashedPassword: string | Buffer = bcrypt.hashSync(value, salt);
                this.setDataValue('password', hashedPassword);
            },
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        country: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        uuid: {
            type: DataTypes.STRING,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false,
        },
        // verification_token: {
        //   type: DataTypes.UUID,
        //   defaultValue: Sequelize.UUIDV4
        // },
        // email_expiry: {
        //   type: DataTypes.DATE
        // },
        email_verified: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        // jwt_token: {
        //   type: DataTypes.STRING
        // },
        // otp: {
        //   type: DataTypes.STRING,
        //   defaultValue: null
        // },
        // otp_expiry: {
        //   type: DataTypes.DATE
        // },
        profile_image: {
            type: DataTypes.STRING,
            defaultValue: null,
            get() {
                const imageUrl = this.getDataValue('profile_image');
                if (imageUrl == null) {
                    return imageUrl;
                }
                return (process.env.AWS_IMAGE_URL as string) + imageUrl;
            },
        },
    },
    {
        sequelize,
        modelName: 'Author',
        tableName: 'authors',
        paranoid: true,
        // hooks: {
        //     beforeCreate: (author) => {
        //         const salt: string = bcrypt.genSaltSync();
        //         // console.log(salt);
        //         author.password = bcrypt.hashSync(author.password!, salt);
        //     },
        // },
        // instanceMethods: {
        //   validPassword: function (password: string) {
        //     return bcrypt.compareSync(password, this.password);
        //   },
        // },
    }
);

// for one - Many Relationship
// Author.hasMany(Book, {
//   foreignKey: 'author_id',
//   sourceKey: 'id'
// });

// Book.belongsTo(Author, {
//   foreignKey: 'author_id',
//   as: 'authors',
//   sourceKey: 'id'
// });
export const booksAuthors = Author.hasMany(BooksAuthors, {
    foreignKey: 'books_id',
    sourceKey: 'id',
    as: 'authorsBooks',
});

Author.belongsToMany(Book, {
    through: {
        model: 'BooksAuthors',
    },
    as: 'books',
    foreignKey: 'authors_id',
    sourceKey: 'id',
});
Book.belongsToMany(Author, {
    through: {
        model: 'BooksAuthors',
    },
    as: 'authors',
    foreignKey: 'books_id',
    sourceKey: 'id',
});
