import { Author } from './authorSchema';
import { BooksAuthors } from './BooksAuthors';

export { Author };
export { BooksAuthors };
