import { Application } from 'express';
import authorRoute from '../components/Author/v1';
import booksRoute from '../components/Books/v1';

export default (app: Application) => {
    app.use(authorRoute);
    app.use(booksRoute);
};
